# YamlCppInterface

This is a small interface package that can be used to inject
`yaml-cpp` as a dependency built alongside an ATLAS style project.

This should be included as a submodule in a self-contained analysis
package, see for example The TopLoop
[ExampleProject](https://gitlab.cern.ch/TopLoop/ExampleProject).

An example code snippet showing how to add `yaml-cpp` as a dependency
for an ATLAS CMake target that is a library:

```cmake
atlas_add_library(YourLibrary
  YourPackage/*.h Root/*.cxx Root/*.h
  PUBLIC_HEADERS YourPackage
  SHARED
  LINK_LIBRARIES SomeOtherLibrary
  PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} YamlCppInterfaceLib
  PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} YamlCppInterfaceLib)

add_dependencies( YourLibrary yaml-cpp)
```

And for an executable:

```cmake
atlas_add_executable(some-executable
  util/some-executable.cxx
  LINK_LIBRARIES ${ROOT_LIBRARIES} YourLibrary YamlCppInterfaceLib)

add_dependencies(some-executable yaml-cpp)
```
